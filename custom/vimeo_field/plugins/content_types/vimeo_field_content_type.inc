<?php

/**
 * @file
 * "Vimeo random video" sample content type. It operates with fetched videos from Vimeo.
 */
/**
 * Declaretion of $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Vimeo videos content type'),
  'description' => t('Vimeo video content type - requires and uses no context.'),
  'single' => TRUE,
  'content_types' => array('vimeo_field_content_type'),
  'render callback' => 'vimeo_field_pane_content_type_render',
  'edit form' => 'vimeo_field_content_type_edit_form',
  'defaults' => array(),
  'category' => array(t('Vimeo video'), -9),
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function vimeo_field_pane_content_type_render($subtype, $conf, $context = NULL) {
  $block = new stdClass();

  // The title actually used in rendering
  $block->title = check_plain("Random videos from Vimeo.");
  $block->content = '';

  if (!empty($conf)) {
    $video_count = $conf['video_count'];
    $size = $conf['thumbnail_size'];

    $query = db_select('field_config', 'fc')
            ->condition('fc.type', 'vimeo', '=')
            ->fields('fc', array('field_name'));
    $result = $query->execute();

    $field_data_tables = array();
    foreach ($result as $record) {
      $field_data_tables[] = $record->field_name;
    }

    if (empty($field_data_tables)) {
      $block->content .= '<p>' . t('You should add Vimeo video field !here first.', array('!here' => l(t('here'), 'admin/structure/types'))) . '</p>';
      return $block;
    }

    foreach ($field_data_tables as $field_name) {
      $table = 'field_data_' . $field_name;
      $field = $field_name . '_value';
      $query = db_select($table, 'f')
              ->fields('f', array($field));
      $result = $query->orderRandom()->range(0, $video_count)->execute();
      $records = array();
      foreach ($result as $record) {
        $records[] = $record;
      }

      if (!empty($records)) {
        $block->content .= t('<h3>@count from %field.</h3>', array(
          '@count' => format_plural(count($records), '1 video', '@count random videos'),
          '%field' => $field_name
        ));

        foreach ($records as $record) {
          $record->{$field};
          $url = 'http://vimeo.com/api/v2/video/' . $record->{$field} . '.php';

          $responce = drupal_http_request($url);
          if ($responce->code == 200) {
            $data = unserialize($responce->data);
          }
          if (!empty($data[0][$size])) {
            $image_url = $data[0][$size];

            // It should be moved into theme template.
            $block->content .= '<p>' . l('<img src="' . $image_url . '"/>', 'http://vimeo.com/' . $record->{$field}, array(
                      'html' => TRUE,
                      'attributes' => array('target' => '_blank')
                    )) . '</p>';
          }
        }
      }
      else {
        $block->content = '<p>' . t('There is no Vimeo videos to display.') . '<br/>';
        $block->content .= t('Please fill some fields for Vimeo video.') . '</p>';
      }
    }
  }

  return $block;
}

/**
 * 'Edit form' callback for the content type.
 */
function vimeo_field_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['video_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Vimeo videos count'),
    '#size' => 32,
    '#description' => t('Enter number of videos to display.'),
    '#default_value' => !empty($conf['video_count']) ? $conf['video_count'] : 3,
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  $form['thumbnail_size'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail Size'),
    '#description' => t('Select size of video Thumbnail'),
    '#default_value' => !empty($conf['thumbnail_size']) ? $conf['thumbnail_size'] : 'thumbnail_small',
    '#options' => array(
      'thumbnail_small' => t('Thumbnail small'),
      'thumbnail_medium' => t('Thumbnail medium'),
      'thumbnail_large' => t('Thumbnail large'),
    ),
  );
  return $form;
}

/**
 * Submit function, note anything in the formstate[conf] automatically gets saved
 */
function vimeo_field_content_type_edit_form_submit(&$form, &$form_state) {
  foreach (array('video_count', 'thumbnail_size') as $value) {
    $form_state['conf'][$value] = $form_state['values'][$value];
  }
}
