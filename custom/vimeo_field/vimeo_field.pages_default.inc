<?php

/**
 * @file
 * This module provides default panel to display 3 random videos.
 */

/**
 * Default panels pages for Vimeo video module
 *
 * @return
 *   Array of pages, normally exported from Panels.
 */
function vimeo_field_default_page_manager_pages() {

  // begin exported panel
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'vimeo_field_base';
  $page->task = 'page';
  $page->admin_title = 'Vimeo videos base page';
  $page->admin_description = 'This panel provide display of 3 random Vimeo video thumbnails.';
  $page->path = 'vimeo_field';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array();
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'pvimeo_field_base_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'vimeo_field_base';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'pipeline' => 'standard',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1';
  $pane->panel = 'middle';
  $pane->type = 'vimeo_field_content_type';
  $pane->subtype = 'vimeo_field_content_type';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'video_count' => '3',
    'thumbnail_size' => 'thumbnail_small',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $display->content['new-1'] = $pane;
  $display->panels['middle'][0] = 'new-1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  // end exported panel.

  $pages['vimeo_video'] = $page;

  return $pages;
}
